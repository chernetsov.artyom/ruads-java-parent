package org.ruads.client.api;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.ruads.client.RuadsProperties;
import org.ruads.client.builder.RequestBuilder;
import org.ruads.client.dto.*;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static java.lang.String.format;
import static java.lang.System.out;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * For test to work:
 * 1. create file: application-secrets.properties under test resources folder
 * 2. add ruads.apiKey=<API-KEY> property with your key, that you need to acquire from
 * <a href="https://ruads.org/">ruads.org</a>
 * more info about api key can be found here:
 * <a href="https://ruads.org/assets/manual/api_30.06.17.pdf">api doc</a>
 * <p>
 * scan, scroll and count tests implemented for Omsk example from api doc, see also ScanRequestBuilderExamplesTest,
 * because if something works for Omsk, it works for everything
 * </p>
 * <p>
 * WARNING! this test will consume your limits
 */
@Ignore
public class RuadsApiImplIntegrationTest {
    private static final String PROPERTIES_RESOURCE = "/application-secrets.properties";

    private RuadsApiImpl api;

    @Before
    public void setUp() {
        api = new RuadsApiImpl(loadProperties());
    }

    @Test
    public void testFindDictionaries() throws Exception {
        List<Dictionary> dictionaries = api.findDictionaries("location");
        out.println(dictionaries);
        assertTrue(!dictionaries.isEmpty());
    }

    @Test
    public void testFindDictionariesByQuery() throws Exception {
        List<Dictionary> dictionaries = api.findDictionaries("location", "Омск");
        out.println(dictionaries);
        assertEquals(1, dictionaries.size());
    }

    @Test
    public void testCount() {
        Dictionary omsk = api.findDictionaries("location", "Омск").get(0);
        LocalDateTime publishedFromDate = LocalDateTime.of(2017, 4, 1, 0, 0);

        CountRequest request = new RequestBuilder()
                .criteria(Criterion.Type.Location)
                    .dictionaryContainsFilter(true, omsk)
                .criteria(Criterion.Type.Phone)
                    .phoneFilter(true)
                .criteria(Criterion.Type.Published)
                    .dateRangeFilter(publishedFromDate)
                .criteria(Criterion.Type.Type)
                    .individualTypeFilter(true)
                .buildCountRequest();

        CountResponse response = api.count(request);
        out.println(response);
        assertTrue(response.getTotalMatch()>0);
    }

    @Test
    public void testScan() {
        Dictionary omsk = api.findDictionaries("location", "Омск").get(0);
        LocalDateTime publishedFromDate = LocalDateTime.of(2017, 4, 1, 0, 0);

        ScanRequest request = new RequestBuilder()
                .criteria(Criterion.Type.Location)
                    .dictionaryContainsFilter(true, omsk)
                .criteria(Criterion.Type.Phone)
                    .phoneFilter(true)
                .criteria(Criterion.Type.Published)
                    .dateRangeFilter(publishedFromDate)
                .criteria(Criterion.Type.Type)
                    .individualTypeFilter(true)
                .buildScanRequest();

        ScrollableResponse response = api.scan(request);
        out.println(response);
        assertTrue(!response.getDocuments().isEmpty());
        assertTrue(response.isHasNext());
        assertNotNull(response.getStatistics());
    }

    @Test
    public void testCountThenScanAndScroll() {
        Dictionary omsk = api.findDictionaries("location", "Омск").get(0);
        LocalDateTime publishedFromDate = LocalDateTime.of(2017, 4, 1, 0, 0);
        // to reduce scrolling
        LocalDateTime publishedToDate = LocalDateTime.of(2017, 4, 1, 4, 0);

        RequestBuilder requestBuilder=new RequestBuilder()
                .criteria(Criterion.Type.Location)
                    .dictionaryContainsFilter(true, omsk)
                .criteria(Criterion.Type.Phone)
                    .phoneFilter(true)
                .criteria(Criterion.Type.Published)
                    .dateRangeFilter(publishedFromDate, publishedToDate)
                .criteria(Criterion.Type.Type)
                    .individualTypeFilter(true);

        // forecast documents amount to load
        CountRequest countRequest = requestBuilder.buildCountRequest();
        CountResponse count = api.count(countRequest);
        out.println(format("forecasting to load %s documents",count));

        ScanRequest scanRequest = requestBuilder.buildScanRequest();
        long documentsLoaded = 0;
        ScrollableResponse response = api.scan(scanRequest);
        out.println(response);

        long totalMatch = response.getStatistics().getTotalMatch();
        documentsLoaded += response.getDocuments().size();

        int scrollingCount=0;
        while (response.isHasNext()){
            out.println(format("scrolling count = %d",++scrollingCount));
            ScrollRequest scrollRequest = new ScrollRequest(response.getScrollId());
            response = api.scroll(scrollRequest);
            out.println(format("documents amount = %d, %s", response.getDocuments().size(), response));

            assertEquals(response.getStatistics().getTotalMatch(), totalMatch);

            documentsLoaded+=response.getDocuments().size();
        }

        assertEquals(documentsLoaded, totalMatch);
        assertEquals(count.getTotalMatch(),totalMatch);

        out.println(format("loaded overall: %d",documentsLoaded));
    }

    /**
     * Use case:
     * Need to load not more then N documents.
     * Expected, that limits will be consumed only for ScanRequest and 1 scroll.
     * Result need to be checked in ruads administer panel
     */
    @Test
    public void testThatLimitsWillBeConsumedOnlyForLoadedDocuments(){
        Dictionary omsk = api.findDictionaries("location", "Омск").get(0);
        // expected huge amount of documents
        LocalDateTime publishedFromDate = LocalDateTime.of(2017, 1, 1, 0, 0);

        RequestBuilder requestBuilder=new RequestBuilder()
                .criteria(Criterion.Type.Location)
                    .dictionaryContainsFilter(true, omsk)
                .criteria(Criterion.Type.Phone)
                    .phoneFilter(true)
                .criteria(Criterion.Type.Published)
                    .dateRangeFilter(publishedFromDate)
                .criteria(Criterion.Type.Type)
                    .individualTypeFilter(true);

        // forecast documents amount to load
        CountRequest countRequest = requestBuilder.buildCountRequest();
        CountResponse count = api.count(countRequest);
        out.println(format("forecasting to load %s documents",count));

        long expectedLimitsConsumed = 2; // for search request

        ScanRequest scanRequest = requestBuilder.buildScanRequest();
        ScrollableResponse response = api.scan(scanRequest);
        out.println(format("documents amount = %d, %s", response.getDocuments().size(), response));
        expectedLimitsConsumed+=response.getDocuments().size();

        ScrollRequest scrollRequest = new ScrollRequest(response.getScrollId());
        response = api.scroll(scrollRequest);
        out.println(format("documents amount = %d, %s", response.getDocuments().size(), response));
        expectedLimitsConsumed+=response.getDocuments().size();

        out.println(format("check that %d limits consumed in ruads administer panel",expectedLimitsConsumed));
    }

    /**
     * Use case:
     * Omsk with word center
     */
    @Test
    public void testThatFullTextSearchWorks(){
        Dictionary omsk = api.findDictionaries("location", "Омск").get(0);
        // expected huge amount of documents
        LocalDateTime publishedFromDate = LocalDateTime.of(2017, 1, 1, 0, 0);

        RequestBuilder requestBuilder=new RequestBuilder()
                .criteria(Criterion.Type.Location)
                    .dictionaryContainsFilter(true, omsk)
                .criteria(Criterion.Type.Text)
                    .fullTextFilter(
                            new RequestBuilder.FullTextFilterBuilder("fillText")
                            .containsWords(Collections.singletonList("центр"))
                    )
                .criteria(Criterion.Type.Phone)
                    .phoneFilter(true)
                .criteria(Criterion.Type.Published)
                    .dateRangeFilter(publishedFromDate)
                .criteria(Criterion.Type.Type)
                    .individualTypeFilter(true);

        ScanRequest scanRequest = requestBuilder.buildScanRequest();
        ScrollableResponse response = api.scan(scanRequest);
        out.println(format("documents amount = %d, %s", response.getDocuments().size(), response));
    }

    private RuadsProperties loadProperties() {
        try {
            String property = new String(
                    Files.readAllBytes(Paths.get(getClass().getResource(PROPERTIES_RESOURCE).toURI()))
            );
            String key = property.split("=")[1];
            //noinspection Convert2Lambda
            return new RuadsProperties() {
                @Override
                public String getApiKey() {
                    return key;
                }
            };
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalStateException(e);
        }

    }

}