package org.ruads.client.api;

import org.ruads.client.RuadsApi;
import org.ruads.client.RuadsProperties;
import org.ruads.client.dto.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static java.lang.String.format;

public class RuadsApiImpl implements RuadsApi {
    private static final URI API_URI = URI.create("https://api.ruads.org/v1/");

    private final RestTemplate restTemplate = new RestTemplate();
    private final HttpHeaders headers;

    public RuadsApiImpl(RuadsProperties properties) {
        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", format("RUADS_API_KEY %s", properties.getApiKey()));
    }

    public List<Dictionary> findDictionaries(String name) {
        return findDictionaries(name, null);
    }

    public List<Dictionary> findDictionaries(String name, String query) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUri(API_URI)
                .path("dictionary/")
                .path(name);
        Optional.ofNullable(query).ifPresent(q -> builder.queryParam("query", q));

        HttpEntity<?> request = new HttpEntity<>(headers);

        HttpEntity<Dictionary[]> response = restTemplate.exchange(
                builder.build().encode().toUri(),
                HttpMethod.GET,
                request,
                Dictionary[].class
        );
        return Arrays.asList(response.getBody());
    }

    @Override
    public ScrollableResponse scan(ScanRequest scanRequest) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUri(API_URI)
                .path("search/")
                .path("scan");

        HttpEntity<ScanRequest> request = new HttpEntity<>(scanRequest, headers);

        HttpEntity<ScrollableResponse> response = restTemplate.exchange(
                builder.build().encode().toUri(),
                HttpMethod.POST,
                request,
                ScrollableResponse.class
        );
        return response.getBody();
    }

    @Override
    public ScrollableResponse scroll(ScrollRequest scrollRequest) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUri(API_URI)
                .path("search/")
                .path("scroll");

        HttpEntity<ScrollRequest> request = new HttpEntity<>(scrollRequest, headers);

        HttpEntity<ScrollableResponse> response = restTemplate.exchange(
                builder.build().encode().toUri(),
                HttpMethod.POST,
                request,
                ScrollableResponse.class
        );
        return response.getBody();
    }

    @Override
    public CountResponse count(CountRequest countRequest) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUri(API_URI)
                .path("search/")
                .path("count");

        HttpEntity<CountRequest> request = new HttpEntity<>(countRequest, headers);

        HttpEntity<CountResponse> response = restTemplate.exchange(
                builder.build().encode().toUri(),
                HttpMethod.POST,
                request,
                CountResponse.class
        );
        return response.getBody();
    }
}
