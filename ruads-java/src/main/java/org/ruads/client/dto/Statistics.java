package org.ruads.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Statistics {
    private final long totalMatch;
    private final long limitsConsumed;
    private final long timeOfExecution;

    public Statistics(
            @JsonProperty("totalMatch") long totalMatch,
            @JsonProperty("limitsConsumed") long limitsConsumed,
            @JsonProperty("timeOfExecution") long timeOfExecution
    ) {
        this.totalMatch = totalMatch;
        this.limitsConsumed = limitsConsumed;
        this.timeOfExecution = timeOfExecution;
    }

    public long getTotalMatch() {
        return totalMatch;
    }

    public long getLimitsConsumed() {
        return limitsConsumed;
    }

    public long getTimeOfExecution() {
        return timeOfExecution;
    }

    @Override
    public String toString() {
        return "Statistics{" +
                "totalMatch=" + totalMatch +
                ", limitsConsumed=" + limitsConsumed +
                ", timeOfExecution=" + timeOfExecution +
                '}';
    }
}
