package org.ruads.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Dictionary {
    private final String id;
    private final String name;
    private final String parentName;

    public Dictionary(
            @JsonProperty("id") String id,
            @JsonProperty("name") String name,
            @JsonProperty("parentName") String parentName
    ) {
        this.id = id;
        this.name = name;
        this.parentName = parentName;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getParentName() {
        return parentName;
    }

    @Override
    public String toString() {
        return "Dictionary{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", parentName='" + parentName + '\'' +
                '}';
    }
}
