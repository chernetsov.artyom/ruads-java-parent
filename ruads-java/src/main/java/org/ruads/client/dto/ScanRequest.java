package org.ruads.client.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Objects;

public class ScanRequest {
    public final List<Criterion> criteria;

    @JsonCreator
    public ScanRequest(@JsonProperty("criteria") List<Criterion> criteria) {
        this.criteria = criteria;
    }

    public List<Criterion> getCriteria() {
        return criteria;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ScanRequest request = (ScanRequest) o;
        return Objects.equals(criteria, request.criteria);
    }

    @Override
    public int hashCode() {
        return Objects.hash(criteria);
    }

    @Override
    public String toString() {
        return "ScanRequest{" +
                "criteria=" + criteria +
                '}';
    }
}
