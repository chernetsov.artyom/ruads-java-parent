package org.ruads.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ScrollRequest {
    private final String scrollId;

    public ScrollRequest(@JsonProperty("scrollId") String scrollId) {
        this.scrollId = scrollId;
    }

    public String getScrollId() {
        return scrollId;
    }

    @Override
    public String toString() {
        return "ScrollRequest{" +
                "scrollId='" + scrollId + '\'' +
                '}';
    }
}
