package org.ruads.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CountResponse {
    private final long totalMatch;
    private final long timeOfExecution;

    public CountResponse(
            @JsonProperty("totalMatch") long totalMatch,
            @JsonProperty("timeOfExecution") long timeOfExecution
    ) {
        this.totalMatch = totalMatch;
        this.timeOfExecution = timeOfExecution;
    }

    public long getTotalMatch() {
        return totalMatch;
    }

    public long getTimeOfExecution() {
        return timeOfExecution;
    }

    @Override
    public String toString() {
        return "CountResponse{" +
                "totalMatch=" + totalMatch +
                ", timeOfExecution=" + timeOfExecution +
                '}';
    }
}
