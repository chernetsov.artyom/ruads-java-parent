package org.ruads.client.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.*;

import static java.lang.String.format;
import static org.ruads.client.dto.Filter.Type.*;
import static org.ruads.client.dto.Filter.Type.Dictionary;

public class Criterion {
    private final String type;
    private final List<Filter> filters;

    @JsonCreator
    private Criterion(@JsonProperty("type") String type, @JsonProperty("filters") List<Filter> filters) {
        this.type = type;
        this.filters = filters;

        if (Objects.isNull(filters) || filters.isEmpty()) {
            throw new IllegalArgumentException("criterium must contain filters");
        }
        checkFiltersAllowed(filters);
    }

    public Criterion(Type type, List<Filter> filters) {
        this(type.getValue(), filters);
    }

    public String getType() {
        return type;
    }

    public List<Filter> getFilters() {
        return filters;
    }

    private void checkFiltersAllowed(List<Filter> filters) {
        Type type = Type.fromValue(this.type);
        boolean filtersAllowed = filters.stream()
                .allMatch(f -> type.allowedFilters.contains(Filter.Type.fromValue(f.getType())));
        if (!filtersAllowed) {
            throw new IllegalArgumentException(format(
                    "criterium %s supports only %s filter, but %s was passed to constructor",
                    this.type,
                    type.allowedFilters,
                    filters
            ));
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Criterion criterion = (Criterion) o;
        return Objects.equals(type, criterion.type) &&
                Objects.equals(filters, criterion.filters);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, filters);
    }

    @Override
    public String toString() {
        return "Criterion{" +
                "type='" + type + '\'' +
                ", filters=" + filters +
                '}';
    }

    public enum Type {
        Address(FullText),
        Category(Dictionary),
        Contact(FullText),
        Description(FullText),
        Subway(FullText),
        Text(FullText),
        Downloaded(Range),
        Published(Range),
        Location(Dictionary),
        Phone(Arrays.asList(StringMatch, Checkbox, Dictionary)),
        Type(Checkbox),
        Price(Range),
        CarDetails(Arrays.asList(Dictionary, Range)),
        Gender(Checkbox);

        private final String value;
        private final Set<Filter.Type> allowedFilters;

        Type(Filter.Type allowedFilter) {
            this.value = format(".%sCriterion", name());
            this.allowedFilters = Collections.singleton(allowedFilter);
        }

        Type(Collection<Filter.Type> allowedFilter) {
            this.value = format(".%sCriterion", name());
            this.allowedFilters = new HashSet<>(allowedFilter);
        }

        public String getValue() {
            return value;
        }

        public static Type fromValue(String value) {
            return Arrays.stream(Criterion.Type.values())
                    .filter(t -> t.value.equals(value)).findFirst()
                    .orElseThrow(() -> new IllegalArgumentException(format("no type with value %s", value)));
        }
    }
}