package org.ruads.client.builder;

import org.ruads.client.dto.*;
import org.ruads.client.dto.Dictionary;
import org.ruads.client.dto.constants.Gender;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

public class RequestBuilder {
    private final Deque<CriterionBuilder> criterions = new ArrayDeque<>();

    public ScanRequest buildScanRequest() {
        List<Criterion> criterionList = criterions.stream()
                .map(CriterionBuilder::build)
                .collect(Collectors.toList());
        return new ScanRequest(criterionList);
    }

    public CountRequest buildCountRequest() {
        List<Criterion> criterionList = criterions.stream()
                .map(CriterionBuilder::build)
                .collect(Collectors.toList());
        return new CountRequest(criterionList);
    }

    public RequestBuilder criteria(Criterion.Type criteriumType) {
        CriterionBuilder criterionBuilder = new CriterionBuilder(criteriumType);
        this.criterions.add(criterionBuilder);
        return this;
    }

    public RequestBuilder rangeFilter(Long start, Long end) {
        return rangeFilter("range", start, end);
    }

    public RequestBuilder rangeFilter(String name, Long start, Long end) {
        Filter filter = new Filter(
                Filter.Type.Range,
                name,
                start,
                end
        );
        return addLastFilter(filter);
    }

    public RequestBuilder phoneFilter(boolean onlyChecked) {
        Filter filter = new Filter(
                Filter.Type.Checkbox,
                "onlyCheckedPhones",
                Boolean.toString(onlyChecked)
        );
        return addLastFilter(filter);
    }

    public RequestBuilder individualTypeFilter(boolean isIndividual) {
        Filter filter = new Filter(
                Filter.Type.Checkbox,
                "individual",
                Boolean.toString(isIndividual)
        );
        return addLastFilter(filter);
    }

    public RequestBuilder genderFilter(Set<Gender> includeGenders, Set<Gender> excludeGenders) {
        includeGenders.forEach(gender -> {
            Filter filter = new Filter(
                    Filter.Type.Checkbox,
                    gender.getFilterName(),
                    "true"
            );
            addLastFilter(filter);
        });
        excludeGenders.forEach(gender -> {
            Filter filter = new Filter(
                    Filter.Type.Checkbox,
                    gender.getFilterName(),
                    "false"
            );
            addLastFilter(filter);
        });
        return this;
    }

    public RequestBuilder dictionaryFilter(String filterName, Dictionary... dictionaries) {
        Filter filter = new Filter(
                Filter.Type.Dictionary,
                filterName,
                Arrays.stream(dictionaries).map(Dictionary::getId).collect(Collectors.toList())
        );
        return addLastFilter(filter);
    }

    public RequestBuilder dictionaryContainsFilter(boolean contains, Dictionary... dictionaries) {
        Filter filter = new Filter(
                Filter.Type.Dictionary,
                contains ? "contains" : "not_contains",
                Arrays.stream(dictionaries).map(Dictionary::getId).collect(Collectors.toList())
        );
        return addLastFilter(filter);
    }

    public RequestBuilder dateRangeFilter(LocalDateTime rangeStart) {
        return rangeFilter(millisSinceEpoch(rangeStart), null);
    }

    public RequestBuilder fullTextFilter(FullTextFilterBuilder builder) {
        return addLastFilter(builder.build());
    }

    public RequestBuilder dateRangeFilter(LocalDateTime rangeStart, LocalDateTime rangeEnd) {
        return rangeFilter(millisSinceEpoch(rangeStart), millisSinceEpoch(rangeEnd));
    }

    public static final class FullTextFilterBuilder {
        private final String name;
        private List<String> doesNotContainPhrases;
        private List<String> containsAnyPhrase;
        private List<String> containsPhrases;
        private List<String> containsWords;
        private List<String> containsAnyWord;
        private List<String> doesNotContainWords;

        public FullTextFilterBuilder(String name) {
            this.name = name;
        }

        public FullTextFilterBuilder doesNotContainPhrases(List<String> doesNotContainPhrases) {
            this.doesNotContainPhrases = doesNotContainPhrases;
            return this;
        }

        public FullTextFilterBuilder containsAnyPhrase(List<String> containsAnyPhrase) {
            this.containsAnyPhrase = containsAnyPhrase;
            return this;
        }

        public FullTextFilterBuilder containsPhrases(List<String> containsPhrases) {
            this.containsPhrases = containsPhrases;
            return this;
        }

        public FullTextFilterBuilder containsWords(List<String> containsWords) {
            this.containsWords = containsWords;
            return this;
        }

        public FullTextFilterBuilder containsAnyWord(List<String> containsAnyWord) {
            this.containsAnyWord = containsAnyWord;
            return this;
        }

        public FullTextFilterBuilder doesNotContainWords(List<String> doesNotContainWords) {
            this.doesNotContainWords = doesNotContainWords;
            return this;
        }

        private Filter build() {
            return new Filter(
                    Filter.Type.FullText,
                    name,
                    nonNullValue(doesNotContainPhrases),
                    nonNullValue(containsAnyPhrase),
                    nonNullValue(containsPhrases),
                    nonNullValue(containsWords),
                    nonNullValue(containsAnyWord),
                    nonNullValue(doesNotContainWords)
            );
        }

        private static List<String> nonNullValue(List<String> value) {
            return Objects.nonNull(value) ? value : new ArrayList<>();
        }
    }

    private RequestBuilder addLastFilter(Filter filter) {
        if (criterions.isEmpty()) {
            throw new IllegalStateException(
                    "trying to add filter before criteria created; create cretarium first: " +
                            "builder.critetium().filter().build()");
        }
        criterions.getLast().addFilter(filter);
        return this;
    }

    private static long millisSinceEpoch(LocalDateTime ldt) {
        // ruads time is in UTC+3
        return ldt.atZone(ZoneId.of("UTC+3")).toInstant().toEpochMilli();
    }
}
